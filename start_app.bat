@echo off
cd /d "%~dp0"  REM Go to the directory where the batch file is located

REM Start the Python backend server
start "" python "Back_server.py"

REM Wait for a few seconds to allow the server to start (adjust the timeout if needed)
timeout /t 1

REM Go to the appli folder and start npm
cd appli
npm start