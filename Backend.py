import json
import os
import matplotlib.pyplot as plt
from datetime import datetime
import numpy as np
import matplotlib
import time
matplotlib.use('agg')
from backend_var_creation import gen_data, get_exo_name, assos, les_mois, get_exo_code

 
# Fonctions auxiliaires

def contient(tab, key):
    for i in tab:
        if i==key:
            return True
    return False

def dictFromJson(filename):
    assert (filename[-5:len(filename)]=='.json'), ("le fichier en entrée n'est pas un .json")
    with open(filename, 'r') as openfile:
        dico = (json.load(openfile))
        openfile.close()
        return dico
    
def jsonFromDict(dict, filename):
    json_object = json.dumps(dict, indent=4)
    assert (filename[-5:len(filename)]=='.json'), ("le fichier en entrée n'est pas un .json")
    with open(filename, 'w') as outfile:
        outfile.write(json_object)
        outfile.close()


# Fonctions principales



def ecrire_seance(filename, date, type, exo, poids_espaces, reps):
    poids = poids_espaces.split('/')
    file = open(filename, 'r')
    seances = json.load(file)
    file.close()
    if not(contient(seances.keys(),date)):
        seances[date] = {}
    seances[date]['type'] = type
    seances[date][exo] = {}
    seances[date][exo]['poids'] = poids
    seances[date][exo]['reps'] = reps
    json_object = json.dumps(seances, indent=4)
    with open(filename, 'w') as outfile:
        outfile.write(json_object)
        outfile.close()

# jsonFromDict({}, 'seances.json')
# ecrire_seance('seances.json','11-11-2023','4','exo','16 /15000','7')

def recup_poids(filename, exo):
    seances = dictFromJson(filename)
    dico_out = {}
    for date in seances.keys():
        if exo in seances[date].keys():
            dico_out[date] = seances[date][exo]['poids']
    return dico_out

def get_seance(filename, date):
    seances = dictFromJson(filename)
    assert contient(seances.keys(), date), "Il n'y a pas eu de séance ce jour-ci"
    return seances[date]

def get_planning(filename):
    seances = dictFromJson(filename)
    dico_out = {}
    for date in seances.keys():
        dico_out[date] = seances[date]['type']
    return dico_out

def create_save(filename):
    seances = dictFromJson(filename)
    index = 1
    while os.path.exists("./saves/" + filename[0:-5] + "_save_" + str(index) + ".json"):
        index +=1
    jsonFromDict(seances, "./saves/" + filename[0:-5] + "_save_" + str(index) + ".json")


# Affichage / tracé

def tracer_poids(filename, exo):
    path = 'appli/public/temp_images/temp.png'
    dico_poids = recup_poids(filename, exo)
    poids_echec = []
    poids = []
    dates_echec = []
    dates = []
    hauteur = 50
    for key in list(dico_poids):
        temp = dico_poids[key]
        pm = 0
        dt = 0
        for i in temp:
            if int(i) > 1000:
                converted = datetime.strptime(key, "%d-%m-%Y").date()
                dates_echec.append(converted)
                poids_echec.append(int(i)/1000)
            elif int(i)>pm:
                converted = datetime.strptime(key, "%d-%m-%Y").date()
                pm = int(i)
                dt = converted
        if pm>0:
            dates.append(dt)
            poids.append(pm)
    max_r = 0
    max_e = 0
    if len(poids)>0:
        max_r = max(poids)
    if len(poids_echec)>0:
        max_r = max(poids_echec)
    uplim = (max(max_r, max_e)//hauteur+1)*hauteur
    fig, ax = plt.subplots( nrows=1, ncols=1 )  # create figure & 1 axis
    ax.set_ylim(0, uplim)
    ax.scatter(dates, poids, color = 'blue')
    ax.plot(dates, poids, color = 'blue')
    ax.scatter(dates_echec, poids_echec, color = 'red', marker = '+')
    ax.set_ylabel('poids (kg)')
    fig.autofmt_xdate() #make dates legible
    ax.set_title('Évolution pour : ' + get_exo_name[exo])
    if os.path.exists(path):
        os.remove(path)
    fig.savefig(path, bbox_inches='tight')   # save the figure to file
    plt.close(fig)    # close the figure window


def compter_mois(monthnum, year, filename):
    """Renvoie les listes des séances et des exos.
     Entrée : monthnum (int) """
    dico = dictFromJson(filename)
    seances = []
    exos = []
    for key in dico:
        date = datetime.strptime(key, "%d-%m-%Y").date()
        if date.month == monthnum and date.year == year:
            seances.append(dico[key]['type'])
            for exo in dico[key]:
                if exo != 'type':
                    exos.append(get_exo_name[exo]) 
    return seances, exos

def creer_hist(objets, nom, xlabel, ylabel, title):
    """Sauvegarde l'histogramme. Objets est une liste, tout le reste doit être des chaînes de caractère"""
    assert type(nom) == str, "Veuillez entrer un nom valide"
    path = './appli/public/temp_images/' + nom + ".png"
    objets = np.array(objets)
    objets_uniques = np.unique(objets)
    perf = [0 for i in objets_uniques]
    for i in range(len(objets_uniques)):
        for el in objets:
            if el == objets_uniques[i]:
                perf[i] += 1
    comptage = {objets_uniques[i]: perf[i] for i in range(len(perf))}
    objets_uniques = sorted(objets_uniques, key=(lambda x : comptage[x]), reverse = True)
    perf = sorted(perf, reverse = True)


    fig, ax = plt.subplots()
    colors = np.random.rand(len(objets_uniques), 3)
    ax.bar(objets_uniques, perf, align='center', alpha=0.7, color=colors)

    # Customize the chart style
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    ax.grid(axis='y', linestyle='--', alpha=0.5)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    plt.xticks(rotation=45, ha='right')
    plt.tight_layout()
    for i, xtick_label in enumerate(ax.get_xticklabels()):
        xtick_label.set_color(colors[i])

    if os.path.exists(path):
        os.remove(path)
    fig.savefig(path, bbox_inches='tight')   # save the figure to file
    plt.close(fig)    # close the figure window


def month_bc(mois, year, filename):
    """ Crée les histogrammes pour séances et exos """
    seances, exos = compter_mois(mois, year, filename)
    creer_hist(seances, "seances", "Séance", "Nombre", "Séances en " + les_mois[mois-1] + " " + str(year))
    creer_hist(exos, "exos", "Exos", "Nombre", "Exos en " + les_mois[mois-1] + " " + str(year))
    
def purger_recap():
    for i in range(1,13):
        path = './appli/public/temp_images/exos_' + str(i) + ".png"
        if os.path.exists(path):
            os.remove(path)
        path = './appli/public/temp_images/seances_' + str(i) + ".png"
        if os.path.exists(path):
            os.remove(path)
        path = './appli/public/temp_images/piechart_exos.png'
        if os.path.exists(path):
            os.remove(path)
        path = './appli/public/recap.json'
        if os.path.exists(path):
            os.remove(path)


def compter_exos(mois, year, filename):
    exos = compter_mois(mois, year, filename)[1]
    travail = {'dos': 0, 'épaules': 0, 'pecs': 0, 'triceps': 0, 'biceps': 0, "abdos":0,'jambes': 0}
    for exo in exos:
        if get_exo_code[exo] not in assos:
            print("ça a fort foiré mon frérot, car l'exo " + exo + " n'est pas dans le dico assos")
        for muscle in assos[get_exo_code[exo]]:
            travail[muscle] += assos[get_exo_code[exo]][muscle]
    return travail

def camembert_exos(mois, year, filename):
    dico = compter_exos(mois, year, filename)
    labels = [key for key in dico]

    sizes = [dico[key] for key in dico]
    colors = np.random.rand(len(labels), 3)

    plt.pie(sizes, labels=labels, colors=colors, 
            autopct='%1.1f%%', shadow=False, startangle=90)
    
    plt.title("Répartition des groupes musculaires travaillés en " + les_mois[mois-1] + " " + str(year))

    plt.axis('equal')

    path = './appli/public/temp_images/piechart_exos.png'
    if os.path.exists(path):
        os.remove(path)
    plt.savefig(path)
    plt.close()
    total_num = sum(sizes)
    return int(total_num)
    
# camembert_exos(3,"seances.json")

def progression_pr(exo_in, mois, year, filename):
    dico = dictFromJson(filename)
    stopbool = False
    max_perf = [0]
    reps = [0]
    initial = [0]
    improve = 0
    for key in dico:
        date = datetime.strptime(key, "%d-%m-%Y").date()
        if date.month == mois and date.year == year:
            for exo in dico[key]:
                if exo==exo_in:
                    poids = dico[key][exo]["poids"]
                    valid = []
                    for i in poids:
                        if int(i)<1000:
                            valid.append(int(i))
                    if len(valid)>0 and not stopbool:
                        initial[0] = max(valid)
                        stopbool = True
                    if len(valid)>0:
                        max_perf[0] = max(max_perf[0], max(valid))
                        reps[0] = dico[key][exo]['reps']
    if initial[0] != 0:
        improve = round((max_perf[0]-initial[0])/initial[0]*100, 1)
    return {'PR' : max_perf[0], 'reps' : int(reps[0]), 'improvement': str(improve) + " %"}

# print(progression_pr('dc', 4, 'seances.json'))
# camembert_exos(4,2023, 'seances.json')

def progression_pr_exos(seuil, mois, year, filename):
    dico = dictFromJson(filename)
    exos = {}
    dic_out = {}
    for key in dico:
        date = datetime.strptime(key, "%d-%m-%Y").date()
        if date.month == mois and date.year == year:
            for exo in dico[key]:
                if exo!= 'type':
                    if exo in exos:
                        exos[exo] += 1
                    else:
                        exos[exo] = 0
    for exo in exos:
        if exos[exo] >= seuil:
            dic_out[get_exo_name[exo]] = progression_pr(exo, mois, year, filename)
    return dic_out

# print(progression_pr_exos(3,4, 2023,"seances.json"))

def total_recap_mois(mois, year, filename, seuil = 2):
    """ Renvoie toutes les informations nécessaires au récap du mois"""
    month_bc(mois,year, filename)
    nb_exos = camembert_exos(mois, year, filename)
    table_progression = progression_pr_exos(seuil, mois, year, filename)
    series_echouees = 0
    reps_reussies = 0
    dico = dictFromJson(filename)
    dic_out = {}
    for key in dico:
        date = datetime.strptime(key, "%d-%m-%Y").date()
        if date.month == mois and date.year == year:
            for exo in dico[key]:
                if exo!= 'type':
                    for serie in dico[key][exo]["poids"]:
                        if int(serie)>=1000:
                            series_echouees+=1
                        else:
                            reps_reussies += int(dico[key][exo]["reps"])
    dic_out["nbexos"] = nb_exos
    dic_out["reps"] = reps_reussies
    dic_out["echecs"] = series_echouees
    dic_out["progression"] = table_progression
    jsonFromDict(dic_out, './appli/public/recap.json')
    jsonFromDict({}, './appli/public/recap_validation.json')
    time.sleep(1)
    if os.path.exists('./appli/public/recap_validation.json'):
        os.remove('./appli/public/recap_validation.json')

total_recap_mois(3,2023, 'seances.json')