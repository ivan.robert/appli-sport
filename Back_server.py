import json
import os
import matplotlib.pyplot as plt
from datetime import datetime
from flask import Flask
from flask import jsonify, request
import requests
from Backend import *
import matplotlib
matplotlib.use('agg')


 
# Démarrage du serveur
app = Flask(__name__)



@app.route('/fichier/creer', methods=['POST'])
def new_file():
    values = json.loads(request.data, strict=False)
    jsonFromDict({}, values['name'])
    response = {'message': 'New dataset created'}
    return jsonify(response), 201

@app.route('/seance/ajouter', methods=['POST'])
def ajout():
    data = json.loads(request.data, strict=False)
    filename = data['filename']
    date = data['date']
    type = data['type']
    exo = data['exo']
    poids = data['poids']
    reps = data['reps']
    ecrire_seance(filename,date,type,exo,poids,reps)
    response = {'message': 'Séance ajoutée avec succès'}
    return jsonify(response), 201

@app.route('/progression', methods = ['POST'])
def graphe_poids():
    data = json.loads(request.data, strict=False)
    filename = data['filename']
    exo = data['exo']
    tracer_poids(filename, exo)
    response = {'message': 'Graphe mis à jour'}
    return jsonify(response), 201

@app.route('/fichier/sauvegarder', methods=['POST'])
def sauvegarde():
    values = json.loads(request.data, strict=False)
    create_save(values['filename'])
    response = {'message': 'Saved'}
    return jsonify(response), 201

@app.route('/recap', methods = ['POST'])
def recap_du_mois():
    data = json.loads(request.data, strict=False)
    filename = data['filename']
    month = int(data['month'])
    year = int(data['year'])
    purger_recap()
    total_recap_mois(month, year, filename)
    response = {'message': 'Recap loaded'}
    return jsonify(response), 201

@app.route('/purge', methods = ['GET'])
def purge():
    purger_recap()
    response = {'message': 'Recap loaded'}
    return jsonify(response), 201



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)



    










