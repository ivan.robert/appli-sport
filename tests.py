import unittest
from Backend import *
import os
# from test import support

class Tests_initiaux(unittest.TestCase):

    # Only use setUp() and tearDown() if necessary
    rank = 1

    def setUp(self):
        print("--- Début du test "+ str(self.rank) + ' ---')
    def tearDown(self):
        print("--- Test "+ str(self.rank) + ' fini ---')
        Tests_initiaux.rank +=1
        

    def test_lecture(self): #1
        jsonFromDict({},'test.json')
        dico = dictFromJson('test.json')
        os.remove("test.json")
        assert dico == {}

    def test_ecriture(self):#2
        dico = {'test':1}
        jsonFromDict(dico, 'write_test.json')
        test_dict = dictFromJson('write_test.json')
        assert os.path.exists("write_test.json")
        os.remove('write_test.json')
        assert dico==test_dict

    def test_ecriture_seance(self):#3
        filename = 'test_ws.json'
        jsonFromDict({},filename)
        ecrire_seance(filename, '1','type','ex','poids','reps')
        dico = dictFromJson('test_ws.json')
        assert dico == {"1": {"type": "type","ex": {"poids": ["poids"],"reps": "reps"}}}
        os.remove('test_ws.json')

    def test_get_poids(self):#4
        filename = 'test_gp.json'
        jsonFromDict({},filename)
        ecrire_seance(filename, '1','type','DC','30','reps')
        ecrire_seance(filename, '2','type','parasite','couille','reps')
        ecrire_seance(filename, '2','type','DC','50','reps')
        assert recup_poids(filename, 'DC') == {'1': ['30'], '2': ['50']}
        os.remove('test_gp.json')

    def test_get_seance(self):#5
        filename = 'test_gs.json'
        jsonFromDict({},filename)
        ecrire_seance(filename, '1','dos','tirage_vertical','30','reps')
        ecrire_seance(filename, '2','type','parasite','couille','reps')
        seance = get_seance(filename, '1')
        assert seance == {"type": "dos","tirage_vertical": {"poids": ["30"],"reps": "reps"}}
        os.remove('test_gs.json')

    def test_planning(self): #6
        filename = 'test.json'
        jsonFromDict({},filename)
        ecrire_seance(filename, '1','dos','tirage_vertical','30','reps')
        ecrire_seance(filename, '2','type','parasite','couille','reps')
        assert get_planning(filename) == {'1':'dos', '2': 'type'}
        os.remove('test.json')

    def test_save(self): #7
        filename = 'test.json'
        jsonFromDict({},filename)
        ecrire_seance(filename, '1','dos','tirage_vertical','30','reps')
        create_save(filename)
        assert os.path.exists("./saves/test_save_1.json")
        dico1 = dictFromJson("./saves/test_save_1.json")
        assert dico1 == {"1": {"type": "dos","tirage_vertical": {"poids": ["30"],"reps": "reps"}}}
        ecrire_seance(filename, '2','type','parasite','couille','reps')
        create_save(filename)
        assert os.path.exists("./saves/test_save_2.json")
        dico2 = dictFromJson("./saves/test_save_2.json")
        assert dico2 != dico1
        os.remove("./saves/test_save_1.json")
        os.remove("./saves/test_save_2.json")
        os.remove('test.json')

    def test_graphe(self): #8
        filename = 'test_graphe.json'
        jsonFromDict({},filename)
        ecrire_seance(filename, '12-03-2023','dos','tirage_vertical','30','reps')
        ecrire_seance(filename, '17-03-2023','dos','tirage_vertical','40','reps')
        ecrire_seance(filename, '17-06-2023','dos','tirage_vertical','20','reps')
        tracer_poids(filename, "tirage_vertical")
        assert True
        os.remove('test_graphe.json')

        
        




    


if __name__ == '__main__':
    unittest.main()