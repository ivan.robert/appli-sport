import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import {AddData} from './components/submit';
import { Home } from './components/Menu';
import RequestDisplay from './components/display_graph';
import DisplayRecap from './components/month_recap';
import RequestRecap from './components/request_recap';


function App() {
  return (
    <BrowserRouter>
    <Routes>
      <Route exact path="/" element={<Home/>} />
      <Route path="/ajout" element={<AddData/>} />
      <Route path="/graphe" element={<RequestDisplay/>} />
      <Route path="/recap/:month" element={<DisplayRecap/>} />
      <Route path="/recapform" element={<RequestRecap/>} />
    </Routes>
    </BrowserRouter>
  );
}

export {App};