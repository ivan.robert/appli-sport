import React, { useState } from "react";
import { Grow } from '@mui/material'
import Select from 'react-select';
import Navbar from "./navbar";



function RequestRecap() {
    let date = new Date()
    let year = date.getFullYear()
    console.log(year)

    const monthoptions = [
        { value: '1', label: 'Janvier' },
        { value: '2', label: 'Février' },
        { value: '3', label: 'Mars' },
        { value: '4', label: 'Avril' },
        { value: '5', label: 'Mai' },
        { value: '6', label: 'Juin' },
        { value: '7', label: 'Juillet' },
        { value: '8', label: 'Août' },
        { value: '9', label: 'Septembre' },
        { value: '10', label: 'Octobre' },
        { value: '11', label: 'Novembre' },
        { value: '12', label: 'Décembre' }
    ]

    const [month, setMonth] = useState('legende');
    const handleMonth = (selected) => { setMonth(selected.value) }

    function create_recap() {
        const filename = document.getElementById("fname").value;
        const mois = month
        const annee = document.getElementById("send_year").value;
        fetch( "http://127.0.0.1:5000/recap", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
    
            },
            mode: 'no-cors',
            body: JSON.stringify({
                "filename":filename,
                "month":mois,
                "year":annee
            })
        }).then(response => console.log(response.data)
            ).catch(function (error) {
                console.error(error)})
            };

            return(
                <><Navbar /><><div><h1 className='header' style={{ "textAlign": "center", fontSize: "1cm", color: 'black' }}>Récap du mois</h1></div><div id="form-box">
                    <table id="add-category-table">
                    <tbody>
                        <tr>
                            <td className='middle-add-category'>
                                <div id="box_title"><h1 style={{ color: 'black' }}>Renseigner les informations ici</h1></div>
                                <div className="cat-name"><Grow in={true} timeout={500}><span><strong>Sauvegarder le fichier</strong><br /> </span></Grow></div>
                                <div className='input-param'>
                                    <input type="text" placeholder="Nom du fichier" id="fname" name="filename" />
                                </div>
                                <div>
                                    <Select options={monthoptions} placeholder="Mois" id='send_month' onChange={handleMonth} />
                                </div>

                                <div className='input-param'>
                                    <input type="text" value = {year} placeholder="Année" id="send_year" name="filename_for_save" />
                                </div>
                                <div>
                                    <input type="reset" value="Go !" onClick={() => {window.location.href = '/recap/' + month; create_recap();
                                      } }></input>
                                </div>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div></></>
            )
}

export default RequestRecap;