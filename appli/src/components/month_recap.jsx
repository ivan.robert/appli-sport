import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Navbar from "./navbar";
import '../styles/recap.css'
import Loading from "./loading";



function DisplayRecap() {

    const { month } = useParams();
    console.log(month)
    const mois = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Obctobre", "Novembre", "Décembre"]
    const month_str = mois[Number(month)-1]
    let header = "Récap du mois de "
    if (Number(month) === 4 || Number(month) === 8) {
        header = "Récap du mois d'"
    }
  const [data, setData] = useState(null);



  useEffect(() => {
    async function fetchData() {
      try{
      const response = await fetch('../recap.json');
      const jsonData = await response.json();
      setData(jsonData);
    } catch (error) {
    setTimeout(fetchData, 10);
     }};
     fetchData();
  }, []);

  
  if (!data) {
    return <Loading/>;
  }
  else {
    
  return (
    <><Navbar /><><div>
      <h1 className="h1recap"> {header} {month_str}</h1>
      <h2 className="h2recap">Statistiques</h2>
    </div><><div className="images">


    <img src={"../temp_images/piechart_exos.png"} alt="pie" className="pie" />
    <img src={"../temp_images/seances.png"} alt="seances" className="seances" />
    <img src={"../temp_images/exos.png"} alt="exos" className="exos" />


    </div>
        <h2 className="h2recap"> Progression sur les exos les plus faits</h2><table className="rtable">
          <thead className="thead_r">
            <tr className="tr">
              <th className="th">Exercice</th>
              <th className="th">PR</th>
              <th className="th">Reps</th>
              <th className="th">Improvement</th>
            </tr>
          </thead>
          <tbody>
            {Object.entries(data.progression).map(([exercise, stats]) => (
              <tr className="tr" key={exercise}>
                <td className="td">{exercise}</td>
                <td className="td">{stats.PR}</td>
                <td className="td">{stats.reps}</td>
                <td className="td">{stats.improvement}</td>
              </tr>
            ))}
          </tbody>
        </table>
        <h2 className="h2recap">Stats diverses</h2>
        <div className="donees-nulles">
          <p className="rp">Nombre d'exos au total : {data.nbexos}</p>
          <p className="rp">Nombre total de reps : {data.reps}</p>
          <p className="rp">Nombre d'échecs : {data.echecs}</p>
        </div></></></>
  );
}
}

export default DisplayRecap;