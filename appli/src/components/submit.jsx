import { Grow } from '@mui/material'
import '../styles/submit.css'
import Navbar from './navbar';
import { useState, useEffect } from 'react';
import Select from 'react-select';




function AddData() {
    const [exos, setExos] = useState(null)
    const [seances, setSeances] = useState(null)
    useEffect(() => {
        async function fetchData() {
          const response = await fetch('../gen_data.json');
          const jsonData = await response.json();
          const chacal = []
          for (let i = 0; i < jsonData.length; i++) {
            chacal.push({ value: jsonData["exos"][i]["value"], label: jsonData["exos"][i]["label"] });
          }
          jsonData["exos"].sort((a, b) => a.label.localeCompare(b.label));
          setExos(jsonData["exos"])
          jsonData["seances"].sort((a, b) => a.label.localeCompare(b.label));
          setSeances(jsonData["seances"])
        }
        fetchData();
      }, []);
    

    const [saved, setSaved] = useState(false);

    const [exo_s, setExo] = useState('legende');
    const [type_s, setType] = useState('legende');

    const handleExo = (selected) => { setExo(selected.value) }
    const handleType = (selected) => { setType(selected.value) }
      


    function send() {
        const filename = document.getElementById("send_fname").value;
        const date = document.getElementById("send_date").value;
        const type_send = type_s;
        const exo = exo_s;
        const poids = document.getElementById("send_poids").value;
        const reps = document.getElementById("send_reps").value;
        fetch( "http://127.0.0.1:5000/seance/ajouter", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',

            },
            mode: 'no-cors',
            body: JSON.stringify({
                "filename":filename,
                "date": date,
                "type": type_send,
                "exo": exo,
                "poids": poids,
                "reps": reps
            })
        }).then(response => console.log(response.data)
            ).catch(function (error) {
                console.log(error)
                return(<p style={{color: 'red', textAlign : 'center'}}>{error}</p>)
            })
}

function sauvegarde() {
    const filename = document.getElementById("save_fname").value;
    fetch( "http://127.0.0.1:5000/fichier/sauvegarder", {
        method: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',

        },
        mode: 'no-cors',
        body: JSON.stringify({
            "filename":filename,
        })
    }).then(response => console.log(response.data)
        ).catch(function (error) {
            console.log(error)
        })
        setSaved(true);
        setTimeout(() => {
            setSaved(false);
          }, 1000);
        };

   
    return(
        <><Navbar /><>
        <div><h1 className='header' style={{ "textAlign": "center", fontSize: "2cm", color: 'black' }}>Ajout de séances</h1></div>
            <div id="form-box">
                <table id="add-category-table">
                    <tr>
                        <td className='middle-add-category'>
                            <div id="box_title"><h1 style = {{color:'black'}}>Renseigner les informations ici</h1></div>
                            <div className="cat-name"><Grow in={true} timeout={500}><span><strong>Pensez à tout remplir</strong><br /> </span></Grow></div>
                            <div className='input-param'>
                                <input type="text" placeholder="Nom du fichier à modifier" id="send_fname" name="filename" />
                            </div>
                            <div>
                                <input type="text" placeholder="date (jj-mm-aaaa)" id="send_date" name="date" />
                            </div>
                            <div>
                                <Select options={seances} placeholder="Séance" id='send_type' onChange={handleType}/>
                            </div>
                            <div>
                                <Select options={exos} placeholder="exo" id='send_exo' onChange={handleExo}/>
                            </div>
                            <div>
                                <input type="text" placeholder="entrer poids séparés par un '/'" id="send_poids" name="poids" />
                            </div>
                            <div>
                                <input type="text" placeholder="nombre de reps par série" id="send_reps" name="reps" />
                            </div>
                            <div><input type="submit" value="Go !" onClick={() => {send();return(<p>Exercice ajouté</p>)}}></input></div>
                        </td>
                    </tr>
                </table>
            </div>
            <div><h1 className='header' style={{ "textAlign": "center", fontSize: "1cm", color: 'black' }}>Créer une sauvegarde</h1></div>
            <div id="form-box">
                <table id="add-category-table">
                    <tr>
                        <td className='middle-add-category'>
                            <div id="box_title"><h1 style = {{color:'black'}}>Renseigner les informations ici</h1></div>
                            <div className="cat-name"><Grow in={true} timeout={500}><span><strong>Sauvegarder le fichier</strong><br /> </span></Grow></div>
                            <div className='input-param'>
                                <input type="text" placeholder="Nom du fichier à sauvegarder" id="save_fname" name="filename_for_save" />
                            </div>
                            <div><input type="submit" value="Go !" onClick={() => { sauvegarde()}}></input>
                            {saved && <p style={{color: 'green'}}>Sauvegarde créée !</p>}</div>

                        </td>
                    </tr>
                </table>
            </div></></>
                
)
}

export {AddData}