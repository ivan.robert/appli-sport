import React from 'react';
import { Link } from 'react-router-dom';
import '../styles/navbar.css'

function Navbar() {
  return (
    <nav>
        <Link className='link' to="/">Home</Link>
        <Link className='link' to="/ajout">Ajout</Link>
        <Link className='link' to="/graphe">Voir ma progression</Link>
        <Link className='link' to="/recapform">Récap du mois</Link>
    </nav>
  );
}

export default Navbar;