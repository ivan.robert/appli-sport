import React from "react";
import Navbar from "./navbar";
import chad from "../assets/chad.jpg"
import '../styles/Home.css'

function Home() {
    return (
        <><Navbar /><div className="content">
            <div className="title"><h1 className='header' style={{ "textAlign": "center", fontSize: "2cm", color: 'black' }}>CHADGPT</h1></div>
            <img src={chad} alt="Chad" />
        </div></>
    )
}

export {Home}