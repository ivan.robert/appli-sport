import { Grow } from '@mui/material'
import '../styles/submit.css'
import Navbar from './navbar';
import { useState, useEffect } from 'react';
import Select from 'react-select';


function RequestDisplay() {
    const [exos, setExos] = useState(null);
    const [exo_s, setExo] = useState(null);
    const [showImage, setShow] = useState(null);
    const handleExo = (selected) => { setExo(selected.value) }

    useEffect(() => {
        setShow("../temp_images/temp.png");
        async function fetchData() {
          const response = await fetch('../gen_data.json');
          const jsonData = await response.json();
          const chacal = []
          for (let i = 0; i < jsonData.length; i++) {
            chacal.push({ value: jsonData["exos"][i]["value"], label: jsonData["exos"][i]["label"] });
          }
          jsonData["exos"].sort((a, b) => a.label.localeCompare(b.label));
          setExos(jsonData["exos"])
        // Ceci était pour charger les données dans exos et seances
        }
        fetchData();
      }, []);


    function send(e) {
        e.preventDefault(); // prevent default form submission behavior
        const filename = document.getElementById("send_fname").value;
        const exo = exo_s;
        console.log(exo)
    
        fetch( "http://127.0.0.1:5000/progression", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
    
            },
            mode: 'no-cors',
            body: JSON.stringify({
                "filename":filename,
                "exo": exo,
     
            })
        }).then(() => {
            // setShow("../temp_images/temp.png");// replace with actual image URL
        }).catch(function (error) {
            console.log(error)
        });
    }
    


    return(
        <><><Navbar /><>
            <div><h1 className='header' style={{ "textAlign": "center", fontSize: "1cm", color: 'black' }}>Évolution</h1></div>
            <div id="form-box">
                <table id="add-category-table">
                <tbody>
                    <tr>
                        <td className='middle-add-category'>
                            <div className="cat-name"><Grow in={true} timeout={500}><span><strong>Entrer le nom du fichier et de l'exo</strong><br /> </span></Grow></div>
                            <div className='input-param'>
                                <input type="text" placeholder="Nom du fichier" id="send_fname" name="filename" />
                            </div>
                            <div>
                                <Select options={exos} placeholder="exo" id="send_exo" onChange={handleExo} />
                            </div>
                            <div><input type="submit" value="Go !" onClick={send}></input></div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div></></>
            <div className='progression'>
            {showImage && <img src={showImage} alt="okok" />}
            </div>
            </>
)
}
export default RequestDisplay