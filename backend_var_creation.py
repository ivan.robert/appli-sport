import json
def jsonFromDict(dict, filename):
    json_object = json.dumps(dict, indent=4)
    assert (filename[-5:len(filename)]=='.json'), ("le fichier en entrée n'est pas un .json")
    with open(filename, 'w') as outfile:
        outfile.write(json_object)
        outfile.close()
gen_data = {"exos" : [
    { "value": 'elevations lat', "label": 'élévations latérales' },
    { "value": 'pistol_bosu', "label": 'pistol squat bosu' },
    { "value": 'squats guidés', "label": 'squats guidés' },
    { "value": 'leg extension', "label": 'leg extension' },
    { "value": 'biceps poulie', "label": 'biceps poulie' },
    { "value": 'gainage fred', "label": 'gainage fred' },
    { "value": '1 leg press', "label": 'presse à une jambe' },
    { "value": 'bss', "label": 'bulgarial split squats' },
    { "value": 'dips', "label": 'dips' },
    { "value": 'pompes', "label": 'pompes' },
    { "value": 'pompes haut pecs', "label": 'pompes haut pecs' },
    { "value": 'tractions brachial', "label": 'tractions brachial' },
    { "value": 'tractions biceps', "label": 'tractions biceps' },
    { "value": 'di', "label": 'Développé incliné' },
    { "value": 'dc', "label": 'Développé couché' },
    { "value": 'tirage vertical prise large', "label": 'tirage vertical prise large' },
    { "value": 'tirage vertical prise serree', "label": 'tirage vertical prise serree' },
    { "value": 'brachial curl halteres', "label": 'brachial curl halteres' },
    { "value": 'presse', "label": 'presse' },
    { "value": 'adducteurs', "label": 'Brise-pastèque' },
    { "value": 'isquio', "label": 'isquio-jambiers' },
    { "value": 'abducteurs', "label": 'Ouvre-pastèque (abducteurs)' },
    { "value": 'triceps poulie haute', "label": 'triceps poulie haute' },
    { "value": 'triceps poulie basse', "label": 'triceps poulie basse' },
    { "value": 'butterfly', "label": 'Chest fly' },
    { "value": 'di halteres', "label": 'développé incliné halteres' },
    { "value": 'tractions', "label": 'tractions' },
    { "value": 'biceps curl supination', "label": 'biceps curl supination' },
    { "value": 'rear delt', "label": 'rear delt' },
    { "value": 'presse horizontale', "label": 'presse horizontale' },
    { "value": 'mollets', "label": 'mollets' },
    { "value": 'chestpress', "label": 'chest press' },
    { "value": 'haut pecs poulie', "label": 'haut pecs poulie' },
    { "value": 'th', "label": 'tirage horizontal' },
    { "value": 'shoulder press', "label": 'shoulder press' },
    { "value": 'trapezes', "label": 'trapezes (shrugs)' },
    { "value": 'converging chest press', "label": 'converging chest press' },
    { "value": 'bas pecs poulie', "label": 'bas pecs poulie' },
    { "value": 'reverse grip curl barre', "label": 'reverse grip curl barre' },
    { "value": 'd th', "label": 'tirage horizontal divergent' },
    { "value": 'd tv', "label": 'tirage vertical divergent' },
    { "value": 'rowing haltere', "label": 'tirage bûcheron' },
    { "value": 'abaisseur 1 bras', "label": 'abaisseur poulie 1 bras' },
    { "value": 'abaisseur', "label": 'abaisseurs poulie' },
    { "value": 'cv_sp', "label": 'converging shoulder press' },
    { "value": 'biceps curl machine', "label": 'biceps curl machine' },
    { "value": 'squat', "label": 'squat' },
    { "value": 'front_raise', "label": 'front raise' },
    { "value": 'front_twist', "label": 'rotation epaules avant' }
], "seances": [
        { "value": 'pecs', "label": 'pecs/triceps' },
        { "value": 'dos', "label": 'dos/biceps' },
        { "value": 'jambes', "label": 'legday' },
        { "value": 'street', "label": 'street' },
        { "value": 'fullbody', "label": 'fullbody' }
      ]
}

get_exo_name = {}
get_exo_code = {}
for i in range(len(gen_data["exos"])):
    get_exo_name[gen_data["exos"][i]["value"]] = gen_data["exos"][i]["label"]
    get_exo_code[gen_data["exos"][i]["label"]] = gen_data["exos"][i]["value"]


assos = {'elevations lat': {'dos':0.4, 'épaules':0.6 },
    'cv_sp': {'épaules':1 },
    'biceps poulie': {'biceps':1 },
    'pistol_bosu': {'jambes':1 },
    'gainage fred': {'abdos':1 },
    '1 leg press': {'jambes':1 },
    'bss': {'jambes':1 },
    'dips': {'triceps':0.9, 'pecs': 0.1 },
    'pompes': {'pecs':0.7, 'triceps': 0.3 },
    'pompes haut pecs': {'pecs': 0.6, 'triceps' : 0.2, 'épaules':0.1 },
    'tractions brachial': {'biceps': 0.8, 'dos': 0.2 },
    'tractions biceps': {'biceps':0.9, 'abdos' : 0.1},
    'di': {'pecs' : 0.8, 'triceps': 0.2},
    'dc': {'pecs' : 0.8, 'triceps': 0.2},
    'tirage vertical prise large': {'biceps': 0.2, 'dos': 0.8 },
    'tirage vertical prise serree': {'biceps': 0.1, 'dos': 0.9 },
    'brachial curl halteres': {'biceps': 1},
    'presse': {'jambes' :1 },
    'adducteurs': {'jambes' :1 },
    'isquio': {'jambes' :1 },
    'abducteurs': {'jambes' :1 },
    'triceps poulie haute': {'triceps' :1 },
    'triceps poulie basse': {'triceps' :1 },
    'butterfly': {'pecs' :1 },
    'di halteres': {'pecs' :0.9, 'triceps':0.1 },
    'leg extension': {'jambes' :1},
    'tractions': {'dos' :0.9, 'biceps': 0.1 },
    'biceps curl supination': {'biceps' :1 },
    'rear delt': {'dos' :0.9, 'épaules':0.1 },
    'presse horizontale': {'jambes' :1 },
    'mollets': {'jambes' :1 },
    'chestpress': {'pecs' :1 },
    'haut pecs poulie': {'pecs' :1 },
    'squats guidés': {'jambes' :1 },
    'th': {'dos' :0.9, 'biceps':0.1 },
    'shoulder press': {'épaules' :0.9, 'triceps':0.1 },
    'trapezes': {'dos' :1 },
    'converging chest press': {'pecs' :1 },
    'bas pecs poulie': {'pecs' :1 },
    'reverse grip curl barre': {'biceps' :1 },
    'd th': {'dos' :0.9, 'biceps':0.1 },
    'd tv': {'dos' : 0.9, 'biceps':0.1 },
    'rowing haltere' : {'dos': 1},
    'abaisseur 1 bras' : {'épaules' :1 },
    'abaisseur' : {'épaules' :0.6, 'abdos':0.2, "triceps":0.2 },
    'biceps curl machine' : {'biceps' :1 },
     'squat' : {'jambes' :1 },
    'front_raise':{ 'épaules':1},
    'front_twist':{ 'épaules':1}}

les_mois = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Obctobre", "Novembre", "Décembre"]


assert len(gen_data["exos"]) == len(assos), "les données ne correspondent pas"
jsonFromDict(gen_data, "./appli/public/gen_data.json")